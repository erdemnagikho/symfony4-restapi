<?php

namespace App\Controller;

use App\Entity\TaskList;
use App\Repository\TaskListRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ListController extends AbstractFOSRestController
{
    private $taskListRepository;
    private $entityManager;

    public function __construct(TaskListRepository $taskListRepository, EntityManagerInterface $entityManager)
    {
        $this->taskListRepository = $taskListRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @return \FOS\RestBundle\View\View
     */
    public function getListsAction()
    {
        $data = $this->taskListRepository->findAll();
        return $this->view($data, Response::HTTP_OK);
    }

    /**
     * @Rest\RequestParam(name="title", description="Title of list", nullable=false)
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function postListsAction(ParamFetcher $paramFetcher)
    {
        $title = $paramFetcher->get('title');

        if ($title) {
            $list = new TaskList();
            $list->setTitle($title);

            $this->entityManager->persist($list);
            $this->entityManager->flush();

            return $this->view($list, Response::HTTP_CREATED);
        }

        return $this->view(['title' => 'This cannot be null'], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param int $id
     * @return \FOS\RestBundle\View\View
     */
    public function getListAction(int $id)
    {
        $data = $this->taskListRepository->findOneBy(['id' => $id]);

        return $this->view($data, Response::HTTP_OK);
    }

    /**
     * @Rest\FileParam(name="image", description="The background of the list", nullable=false, image=true)
     * @param Request $request
     * @param ParamFetcher $paramFetcher
     * @param $id
     */
    public function backgroundListsAction(Request $request, ParamFetcher $paramFetcher, $id)
    {
        $taskList = $this->taskListRepository->findOneBy(['id' => $id]);
        $currentBackground = $taskList->getBackground();

        if (!is_null($currentBackground)) {
            $fileSystem = new Filesystem();
            $fileSystem->remove($this->getUploadsDir() . $currentBackground);
        }

        /** @var UploadedFile $file */
        $file = $paramFetcher->get('image');

        if ($file) {
            $fileName = md5(uniqid()) . '.' . $file->guessClientExtension();
            $file->move($this->getUploadsDir(), $fileName);

            $taskList->setBackground($fileName);
            $taskList->setBackgroundPath('/uploads/' . $fileName);

            $this->entityManager->persist($taskList);
            $this->entityManager->flush();

            $data = $request->getUriForPath($taskList->getBackgroundPath());

            return $this->view($data, Response::HTTP_CREATED);
        }

        return $this->view(['message' => 'Something went wrong!'], Response::HTTP_BAD_REQUEST);
    }

    private function getUploadsDir()
    {
        return $this->getParameter('uploads_dir');
    }
}
